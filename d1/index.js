//CRUD Operations


//Create
	// - to insert a document/s 

//insertOne method
db.collections.insertOne({document})

db.users.insertOne({

	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@gmail.com"
	},
	courses:['CSS','JavaScript','Python'],
	department: "none"
});

//insertMany() method
db.collections.insertMany([{doc1},{doc2},...]);

db.users.insertMany(
    [
        {
                "firstName": "Stephen",
                "lastName": "Hawking",
                "age": 76,
                "contact": {
                        "phone": "87654321",
                        "email": "stephenhawking@gmail.com"
                },
                "courses": ["Python", "React", "PHP"],
                "department": "none"
        }, 
{
                "firstName": "Neil",
                "lastName": "Armstrong",
                "age": 82,
                "contact": {
                        "phone": "87654321",
                        "email": "neilarmstrong@gmail.com"
                },
                "courses": ["React", "Laravel", "Sass"],
                "department": "none"
        }, 
    ]
);

//Read Operation
	//-retrieves documents from the collection
	//syntax: db.collection.find({query},{field projection})

	//find() method
	db.users.find({firstName},{test});


//Update Operation - update a document/s

//Syntax: db.collection.updateOne()
//db.collections.updateOne({filter},{update})

//updateOne() method
//add a document first to be modified
db.users.insertOne({
	 "firstName": "test",
                "lastName": "test",
                "age": 0,
                "contact": {
                        "phone": "0",
                        "email": "test@gmail.com"
                },
                "courses": [],
                "department": "none"
})

//modifying the added document using updateOne() method

db.users.updateOne({});

//use first name field as a filter and look for the name test
//using update operator set, update the fields of the matching document with the following details 

	//first name : bill
	//last name : gates
	// age : 65
	//phone 12345678
	//email: bill@gmail.com
	//courses: PHP, Laravel, HTML, operations dept, status: active


db.users.updateOne(
	{"firstName": "test"}
	{$set: {
		"firstName": "bill",
		"lastName": "gates",
		"age": 65,
		"contact": {
			"phone": "12345678",
			"email": "bill@gmail.com"
		},
		"courses": ["PHP","Laravel","HTML"],
		"department": "Operations"
		"status": "active"
	}}
);

//updateMany
db.users.updateMany(
{"department":"none"},
{$set: {"department": "HR"}}
);

//mini activity
	//look for a document that has a field status,using object id as a filter
	//remove the status field using update operator

	db.users.updateOne(
	{"_id" : ObjectId("61e7fd72b1ba5f53db1b6e95")},
	{$unset:{"status":"active"}}
	);

//Delete Operation - delete a document/s
	//db.collections.deleteOne({filter})
	//db.collections.deleteMany()

	//insert a document as an example to be deleted
	db.users.insertOne({"firstName":"Joy","lastName":"Pague"})

	//deleteOne() method
	db.users.deleteOne(
	{"firstName":"Joy"}
	);


//insert a document as an example to be deleted

db.users.insertOne({"firstName":"Bill","lastName":"Crawford"});


//deleteMany()
db.users.deleteMany(
 {"firstName":"bill"}
 )
